import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'accord',
        loadChildren: () => import('./accord/accord.module').then(m => m.GestionDesAccordsAccordModule),
      },
      {
        path: 'partenaire',
        loadChildren: () => import('./partenaire/partenaire.module').then(m => m.GestionDesAccordsPartenaireModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class GestionDesAccordsEntityModule {}
