import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPartenaire, Partenaire } from 'app/shared/model/partenaire.model';
import { PartenaireService } from './partenaire.service';
import { IAccord } from 'app/shared/model/accord.model';
import { AccordService } from 'app/entities/accord/accord.service';

@Component({
  selector: 'jhi-partenaire-update',
  templateUrl: './partenaire-update.component.html',
})
export class PartenaireUpdateComponent implements OnInit {
  isSaving = false;
  accords: IAccord[] = [];

  editForm = this.fb.group({
    id: [],
    societe: [null, [Validators.required]],
    domaine: [null, [Validators.required]],
    email: [null, [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    telephone: [null, [Validators.required]],
    pays: [null, [Validators.required]],
    fax: [],
    adresse: [null, [Validators.required]],
    accords: [],
  });

  constructor(
    protected partenaireService: PartenaireService,
    protected accordService: AccordService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ partenaire }) => {
      this.updateForm(partenaire);

      this.accordService.query().subscribe((res: HttpResponse<IAccord[]>) => (this.accords = res.body || []));
    });
  }

  updateForm(partenaire: IPartenaire): void {
    this.editForm.patchValue({
      id: partenaire.id,
      societe: partenaire.societe,
      domaine: partenaire.domaine,
      email: partenaire.email,
      telephone: partenaire.telephone,
      pays: partenaire.pays,
      fax: partenaire.fax,
      adresse: partenaire.adresse,
      accords: partenaire.accords,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const partenaire = this.createFromForm();
    if (partenaire.id !== undefined) {
      this.subscribeToSaveResponse(this.partenaireService.update(partenaire));
    } else {
      this.subscribeToSaveResponse(this.partenaireService.create(partenaire));
    }
  }

  private createFromForm(): IPartenaire {
    return {
      ...new Partenaire(),
      id: this.editForm.get(['id'])!.value,
      societe: this.editForm.get(['societe'])!.value,
      domaine: this.editForm.get(['domaine'])!.value,
      email: this.editForm.get(['email'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      pays: this.editForm.get(['pays'])!.value,
      fax: this.editForm.get(['fax'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      accords: this.editForm.get(['accords'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPartenaire>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IAccord): any {
    return item.id;
  }

  getSelected(selectedVals: IAccord[], option: IAccord): IAccord {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
