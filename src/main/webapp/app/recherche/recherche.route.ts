import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { RechercheComponent } from './recherche.component';

export const RECHERCHE_ROUTE: Route = {
  path: 'recherche',
  component: RechercheComponent,
  data: {
    authorities: [],
    pageTitle: 'recherche.title',
  },
  canActivate: [UserRouteAccessService],
};
