import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestionDesAccordsSharedModule } from '../shared/shared.module';

import { RECHERCHE_ROUTE, RechercheComponent } from './';

@NgModule({
  imports: [GestionDesAccordsSharedModule, RouterModule.forRoot([RECHERCHE_ROUTE], { useHash: true })],
  declarations: [RechercheComponent],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class GestionDesAccordsAppRechercheModule {}
