import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IAccord } from 'app/shared/model/accord.model';
import { AccordService } from '../entities/accord/accord.service';

@Component({
  selector: 'jhi-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['recherche.component.scss'],
})
export class RechercheComponent implements OnInit {
  message: string;
  accords?: IAccord[];

  constructor(protected accordService: AccordService) {
    this.message = 'RechercheComponent message';
  }

  search(critere: string): void {
    this.accordService.search(critere).subscribe((res: HttpResponse<IAccord[]>) => (this.accords = res.body || []));
  }

  ngOnInit(): void {}
}
