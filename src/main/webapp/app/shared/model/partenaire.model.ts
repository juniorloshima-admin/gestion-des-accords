import { IAccord } from 'app/shared/model/accord.model';

export interface IPartenaire {
  id?: number;
  societe?: string;
  domaine?: string;
  email?: string;
  telephone?: number;
  pays?: string;
  fax?: number;
  adresse?: string;
  accords?: IAccord[];
}

export class Partenaire implements IPartenaire {
  constructor(
    public id?: number,
    public societe?: string,
    public domaine?: string,
    public email?: string,
    public telephone?: number,
    public pays?: string,
    public fax?: number,
    public adresse?: string,
    public accords?: IAccord[]
  ) {}
}
