import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IPartenaire } from 'app/shared/model/partenaire.model';

export interface IAccord {
  id?: number;
  titre?: string;
  description?: string;
  dateSignature?: Moment;
  dateExperation?: Moment;
  documentContentType?: string;
  document?: any;
  typeAccord?: string;
  societe?: string;
  status?: boolean;
  user?: IUser;
  partenaires?: IPartenaire[];
}

export class Accord implements IAccord {
  constructor(
    public id?: number,
    public titre?: string,
    public description?: string,
    public dateSignature?: Moment,
    public dateExperation?: Moment,
    public documentContentType?: string,
    public document?: any,
    public typeAccord?: string,
    public societe?: string,
    public status?: boolean,
    public user?: IUser,
    public partenaires?: IPartenaire[]
  ) {
    this.status = this.status || false;
  }
}
