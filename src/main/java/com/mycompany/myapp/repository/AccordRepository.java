package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Accord;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Accord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccordRepository extends JpaRepository<Accord, Long> {

    @Query("select accord from Accord accord where accord.user.login = ?#{principal.username}")
    List<Accord> findByUserIsCurrentUser();

    @Query("select accord from Accord accord where LOWER(accord.societe) =:societe")
    List<Accord> findBySociete(@Param("societe") String societe);
}
