package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Partenaire;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Partenaire entity.
 */
@Repository
public interface PartenaireRepository extends JpaRepository<Partenaire, Long> {

    @Query(value = "select distinct partenaire from Partenaire partenaire left join fetch partenaire.accords",
        countQuery = "select count(distinct partenaire) from Partenaire partenaire")
    Page<Partenaire> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct partenaire from Partenaire partenaire left join fetch partenaire.accords")
    List<Partenaire> findAllWithEagerRelationships();

    @Query("select partenaire from Partenaire partenaire left join fetch partenaire.accords where partenaire.id =:id")
    Optional<Partenaire> findOneWithEagerRelationships(@Param("id") Long id);
}
