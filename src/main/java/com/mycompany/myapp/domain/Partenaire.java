package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Partenaire.
 */
@Entity
@Table(name = "partenaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Partenaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "societe", nullable = false)
    private String societe;

    @NotNull
    @Column(name = "domaine", nullable = false)
    private String domaine;

    @Pattern(regexp = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "telephone", nullable = false)
    private Integer telephone;

    @NotNull
    @Column(name = "pays", nullable = false)
    private String pays;

    @Column(name = "fax")
    private Integer fax;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "partenaire_accord",
               joinColumns = @JoinColumn(name = "partenaire_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "accord_id", referencedColumnName = "id"))
    private Set<Accord> accords = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSociete() {
        return societe;
    }

    public Partenaire societe(String societe) {
        this.societe = societe;
        return this;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public String getDomaine() {
        return domaine;
    }

    public Partenaire domaine(String domaine) {
        this.domaine = domaine;
        return this;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public String getEmail() {
        return email;
    }

    public Partenaire email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelephone() {
        return telephone;
    }

    public Partenaire telephone(Integer telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(Integer telephone) {
        this.telephone = telephone;
    }

    public String getPays() {
        return pays;
    }

    public Partenaire pays(String pays) {
        this.pays = pays;
        return this;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public Integer getFax() {
        return fax;
    }

    public Partenaire fax(Integer fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(Integer fax) {
        this.fax = fax;
    }

    public String getAdresse() {
        return adresse;
    }

    public Partenaire adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Set<Accord> getAccords() {
        return accords;
    }

    public Partenaire accords(Set<Accord> accords) {
        this.accords = accords;
        return this;
    }

    public Partenaire addAccord(Accord accord) {
        this.accords.add(accord);
        accord.getPartenaires().add(this);
        return this;
    }

    public Partenaire removeAccord(Accord accord) {
        this.accords.remove(accord);
        accord.getPartenaires().remove(this);
        return this;
    }

    public void setAccords(Set<Accord> accords) {
        this.accords = accords;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Partenaire)) {
            return false;
        }
        return id != null && id.equals(((Partenaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Partenaire{" +
            "id=" + getId() +
            ", societe='" + getSociete() + "'" +
            ", domaine='" + getDomaine() + "'" +
            ", email='" + getEmail() + "'" +
            ", telephone=" + getTelephone() +
            ", pays='" + getPays() + "'" +
            ", fax=" + getFax() +
            ", adresse='" + getAdresse() + "'" +
            "}";
    }
}
