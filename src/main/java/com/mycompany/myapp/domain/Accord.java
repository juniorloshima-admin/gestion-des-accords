package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Accord.
 */
@Entity
@Table(name = "accord")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Accord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "titre", nullable = false)
    private String titre;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "date_signature", nullable = false)
    private LocalDate dateSignature;

    @NotNull
    @Column(name = "date_experation", nullable = false)
    private LocalDate dateExperation;

    @Lob
    @Column(name = "document")
    private byte[] document;

    @Column(name = "document_content_type")
    private String documentContentType;

    @NotNull
    @Column(name = "type_accord", nullable = false)
    private String typeAccord;

    @NotNull
    @Column(name = "societe", nullable = false)
    private String societe;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    @JsonIgnoreProperties(value = "accords", allowSetters = true)
    private User user;

    @ManyToMany(mappedBy = "accords")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Partenaire> partenaires = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public Accord titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public Accord description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateSignature() {
        return dateSignature;
    }

    public Accord dateSignature(LocalDate dateSignature) {
        this.dateSignature = dateSignature;
        return this;
    }

    public void setDateSignature(LocalDate dateSignature) {
        this.dateSignature = dateSignature;
    }

    public LocalDate getDateExperation() {
        return dateExperation;
    }

    public Accord dateExperation(LocalDate dateExperation) {
        this.dateExperation = dateExperation;
        return this;
    }

    public void setDateExperation(LocalDate dateExperation) {
        this.dateExperation = dateExperation;
    }

    public byte[] getDocument() {
        return document;
    }

    public Accord document(byte[] document) {
        this.document = document;
        return this;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getDocumentContentType() {
        return documentContentType;
    }

    public Accord documentContentType(String documentContentType) {
        this.documentContentType = documentContentType;
        return this;
    }

    public void setDocumentContentType(String documentContentType) {
        this.documentContentType = documentContentType;
    }

    public String getTypeAccord() {
        return typeAccord;
    }

    public Accord typeAccord(String typeAccord) {
        this.typeAccord = typeAccord;
        return this;
    }

    public void setTypeAccord(String typeAccord) {
        this.typeAccord = typeAccord;
    }

    public String getSociete() {
        return societe;
    }

    public Accord societe(String societe) {
        this.societe = societe;
        return this;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public Boolean isStatus() {
        return status;
    }

    public Accord status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public Accord user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Partenaire> getPartenaires() {
        return partenaires;
    }

    public Accord partenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
        return this;
    }

    public Accord addPartenaire(Partenaire partenaire) {
        this.partenaires.add(partenaire);
        partenaire.getAccords().add(this);
        return this;
    }

    public Accord removePartenaire(Partenaire partenaire) {
        this.partenaires.remove(partenaire);
        partenaire.getAccords().remove(this);
        return this;
    }

    public void setPartenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Accord)) {
            return false;
        }
        return id != null && id.equals(((Accord) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Accord{" +
            "id=" + getId() +
            ", titre='" + getTitre() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateSignature='" + getDateSignature() + "'" +
            ", dateExperation='" + getDateExperation() + "'" +
            ", document='" + getDocument() + "'" +
            ", documentContentType='" + getDocumentContentType() + "'" +
            ", typeAccord='" + getTypeAccord() + "'" +
            ", societe='" + getSociete() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
