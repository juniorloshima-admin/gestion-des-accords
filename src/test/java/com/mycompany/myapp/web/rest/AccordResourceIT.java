package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.GestionDesAccordsApp;
import com.mycompany.myapp.domain.Accord;
import com.mycompany.myapp.repository.AccordRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AccordResource} REST controller.
 */
@SpringBootTest(classes = GestionDesAccordsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AccordResourceIT {

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_SIGNATURE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_SIGNATURE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_EXPERATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_EXPERATION = LocalDate.now(ZoneId.systemDefault());

    private static final byte[] DEFAULT_DOCUMENT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_DOCUMENT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_DOCUMENT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_DOCUMENT_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_TYPE_ACCORD = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_ACCORD = "BBBBBBBBBB";

    private static final String DEFAULT_SOCIETE = "AAAAAAAAAA";
    private static final String UPDATED_SOCIETE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private AccordRepository accordRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAccordMockMvc;

    private Accord accord;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Accord createEntity(EntityManager em) {
        Accord accord = new Accord()
            .titre(DEFAULT_TITRE)
            .description(DEFAULT_DESCRIPTION)
            .dateSignature(DEFAULT_DATE_SIGNATURE)
            .dateExperation(DEFAULT_DATE_EXPERATION)
            .document(DEFAULT_DOCUMENT)
            .documentContentType(DEFAULT_DOCUMENT_CONTENT_TYPE)
            .typeAccord(DEFAULT_TYPE_ACCORD)
            .societe(DEFAULT_SOCIETE)
            .status(DEFAULT_STATUS);
        return accord;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Accord createUpdatedEntity(EntityManager em) {
        Accord accord = new Accord()
            .titre(UPDATED_TITRE)
            .description(UPDATED_DESCRIPTION)
            .dateSignature(UPDATED_DATE_SIGNATURE)
            .dateExperation(UPDATED_DATE_EXPERATION)
            .document(UPDATED_DOCUMENT)
            .documentContentType(UPDATED_DOCUMENT_CONTENT_TYPE)
            .typeAccord(UPDATED_TYPE_ACCORD)
            .societe(UPDATED_SOCIETE)
            .status(UPDATED_STATUS);
        return accord;
    }

    @BeforeEach
    public void initTest() {
        accord = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccord() throws Exception {
        int databaseSizeBeforeCreate = accordRepository.findAll().size();
        // Create the Accord
        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isCreated());

        // Validate the Accord in the database
        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeCreate + 1);
        Accord testAccord = accordList.get(accordList.size() - 1);
        assertThat(testAccord.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testAccord.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAccord.getDateSignature()).isEqualTo(DEFAULT_DATE_SIGNATURE);
        assertThat(testAccord.getDateExperation()).isEqualTo(DEFAULT_DATE_EXPERATION);
        assertThat(testAccord.getDocument()).isEqualTo(DEFAULT_DOCUMENT);
        assertThat(testAccord.getDocumentContentType()).isEqualTo(DEFAULT_DOCUMENT_CONTENT_TYPE);
        assertThat(testAccord.getTypeAccord()).isEqualTo(DEFAULT_TYPE_ACCORD);
        assertThat(testAccord.getSociete()).isEqualTo(DEFAULT_SOCIETE);
        assertThat(testAccord.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createAccordWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accordRepository.findAll().size();

        // Create the Accord with an existing ID
        accord.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        // Validate the Accord in the database
        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTitreIsRequired() throws Exception {
        int databaseSizeBeforeTest = accordRepository.findAll().size();
        // set the field null
        accord.setTitre(null);

        // Create the Accord, which fails.


        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = accordRepository.findAll().size();
        // set the field null
        accord.setDescription(null);

        // Create the Accord, which fails.


        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateSignatureIsRequired() throws Exception {
        int databaseSizeBeforeTest = accordRepository.findAll().size();
        // set the field null
        accord.setDateSignature(null);

        // Create the Accord, which fails.


        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateExperationIsRequired() throws Exception {
        int databaseSizeBeforeTest = accordRepository.findAll().size();
        // set the field null
        accord.setDateExperation(null);

        // Create the Accord, which fails.


        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeAccordIsRequired() throws Exception {
        int databaseSizeBeforeTest = accordRepository.findAll().size();
        // set the field null
        accord.setTypeAccord(null);

        // Create the Accord, which fails.


        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSocieteIsRequired() throws Exception {
        int databaseSizeBeforeTest = accordRepository.findAll().size();
        // set the field null
        accord.setSociete(null);

        // Create the Accord, which fails.


        restAccordMockMvc.perform(post("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAccords() throws Exception {
        // Initialize the database
        accordRepository.saveAndFlush(accord);

        // Get all the accordList
        restAccordMockMvc.perform(get("/api/accords?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accord.getId().intValue())))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].dateSignature").value(hasItem(DEFAULT_DATE_SIGNATURE.toString())))
            .andExpect(jsonPath("$.[*].dateExperation").value(hasItem(DEFAULT_DATE_EXPERATION.toString())))
            .andExpect(jsonPath("$.[*].documentContentType").value(hasItem(DEFAULT_DOCUMENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].document").value(hasItem(Base64Utils.encodeToString(DEFAULT_DOCUMENT))))
            .andExpect(jsonPath("$.[*].typeAccord").value(hasItem(DEFAULT_TYPE_ACCORD)))
            .andExpect(jsonPath("$.[*].societe").value(hasItem(DEFAULT_SOCIETE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getAccord() throws Exception {
        // Initialize the database
        accordRepository.saveAndFlush(accord);

        // Get the accord
        restAccordMockMvc.perform(get("/api/accords/{id}", accord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(accord.getId().intValue()))
            .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.dateSignature").value(DEFAULT_DATE_SIGNATURE.toString()))
            .andExpect(jsonPath("$.dateExperation").value(DEFAULT_DATE_EXPERATION.toString()))
            .andExpect(jsonPath("$.documentContentType").value(DEFAULT_DOCUMENT_CONTENT_TYPE))
            .andExpect(jsonPath("$.document").value(Base64Utils.encodeToString(DEFAULT_DOCUMENT)))
            .andExpect(jsonPath("$.typeAccord").value(DEFAULT_TYPE_ACCORD))
            .andExpect(jsonPath("$.societe").value(DEFAULT_SOCIETE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAccord() throws Exception {
        // Get the accord
        restAccordMockMvc.perform(get("/api/accords/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccord() throws Exception {
        // Initialize the database
        accordRepository.saveAndFlush(accord);

        int databaseSizeBeforeUpdate = accordRepository.findAll().size();

        // Update the accord
        Accord updatedAccord = accordRepository.findById(accord.getId()).get();
        // Disconnect from session so that the updates on updatedAccord are not directly saved in db
        em.detach(updatedAccord);
        updatedAccord
            .titre(UPDATED_TITRE)
            .description(UPDATED_DESCRIPTION)
            .dateSignature(UPDATED_DATE_SIGNATURE)
            .dateExperation(UPDATED_DATE_EXPERATION)
            .document(UPDATED_DOCUMENT)
            .documentContentType(UPDATED_DOCUMENT_CONTENT_TYPE)
            .typeAccord(UPDATED_TYPE_ACCORD)
            .societe(UPDATED_SOCIETE)
            .status(UPDATED_STATUS);

        restAccordMockMvc.perform(put("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAccord)))
            .andExpect(status().isOk());

        // Validate the Accord in the database
        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeUpdate);
        Accord testAccord = accordList.get(accordList.size() - 1);
        assertThat(testAccord.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testAccord.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAccord.getDateSignature()).isEqualTo(UPDATED_DATE_SIGNATURE);
        assertThat(testAccord.getDateExperation()).isEqualTo(UPDATED_DATE_EXPERATION);
        assertThat(testAccord.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testAccord.getDocumentContentType()).isEqualTo(UPDATED_DOCUMENT_CONTENT_TYPE);
        assertThat(testAccord.getTypeAccord()).isEqualTo(UPDATED_TYPE_ACCORD);
        assertThat(testAccord.getSociete()).isEqualTo(UPDATED_SOCIETE);
        assertThat(testAccord.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingAccord() throws Exception {
        int databaseSizeBeforeUpdate = accordRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccordMockMvc.perform(put("/api/accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accord)))
            .andExpect(status().isBadRequest());

        // Validate the Accord in the database
        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAccord() throws Exception {
        // Initialize the database
        accordRepository.saveAndFlush(accord);

        int databaseSizeBeforeDelete = accordRepository.findAll().size();

        // Delete the accord
        restAccordMockMvc.perform(delete("/api/accords/{id}", accord.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Accord> accordList = accordRepository.findAll();
        assertThat(accordList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
